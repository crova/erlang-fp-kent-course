-module(first).
-export([double/1, mult/2, area/3, square/1, treble/1]).

% define a function `mult/2`
mult(X,Y) ->
  X*Y.

double(X) ->
  mult(2,X).


%% first:double(8). % to call it
area(A,B,C) ->
  S = (A+B+C)/2,
  math:sqrt(S*(S-A)*(S-B)*(S-C)).

square(A) ->
  A * A.

treble(A) ->
 A * A * A.
