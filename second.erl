-module(second).
-export([hyp/2, d/1, c/1, area/1]).

%% a2 + b2 = c2. We want c.
hyp(A,B) ->
  math:sqrt(first:square(A) + first:square(B)).

%% A = r.
%% d = diameter
%% c = circumference
d(A) ->
  2 * A.

c(A) ->
  3.14 * 2 * A.

area(A) ->
  3.14 * first:square(A).
